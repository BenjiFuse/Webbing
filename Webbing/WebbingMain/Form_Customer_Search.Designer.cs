﻿namespace Webbing
{
    partial class Form_Customer_Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtQuery = new System.Windows.Forms.TextBox();
            this.spltcntrSearch = new System.Windows.Forms.SplitContainer();
            this.lstSuggestions = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.spltcntrSearch)).BeginInit();
            this.spltcntrSearch.Panel1.SuspendLayout();
            this.spltcntrSearch.Panel2.SuspendLayout();
            this.spltcntrSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtQuery
            // 
            this.txtQuery.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtQuery.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuery.Location = new System.Drawing.Point(0, 0);
            this.txtQuery.Name = "txtQuery";
            this.txtQuery.Size = new System.Drawing.Size(512, 29);
            this.txtQuery.TabIndex = 0;
            this.txtQuery.TabStop = false;
            this.txtQuery.WordWrap = false;
            this.txtQuery.TextChanged += new System.EventHandler(this.txtQuery_TextChanged);
            this.txtQuery.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtQuery_KeyDown);
            // 
            // spltcntrSearch
            // 
            this.spltcntrSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltcntrSearch.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spltcntrSearch.IsSplitterFixed = true;
            this.spltcntrSearch.Location = new System.Drawing.Point(0, 0);
            this.spltcntrSearch.Name = "spltcntrSearch";
            this.spltcntrSearch.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltcntrSearch.Panel1
            // 
            this.spltcntrSearch.Panel1.Controls.Add(this.txtQuery);
            // 
            // spltcntrSearch.Panel2
            // 
            this.spltcntrSearch.Panel2.Controls.Add(this.lstSuggestions);
            this.spltcntrSearch.Size = new System.Drawing.Size(512, 512);
            this.spltcntrSearch.SplitterDistance = 25;
            this.spltcntrSearch.TabIndex = 1;
            this.spltcntrSearch.TabStop = false;
            // 
            // lstSuggestions
            // 
            this.lstSuggestions.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lstSuggestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstSuggestions.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstSuggestions.FormattingEnabled = true;
            this.lstSuggestions.ItemHeight = 20;
            this.lstSuggestions.Items.AddRange(new object[] {
            "First",
            "Last",
            "Address",
            "Status"});
            this.lstSuggestions.Location = new System.Drawing.Point(0, 0);
            this.lstSuggestions.Name = "lstSuggestions";
            this.lstSuggestions.Size = new System.Drawing.Size(512, 483);
            this.lstSuggestions.TabIndex = 0;
            this.lstSuggestions.SelectedIndexChanged += new System.EventHandler(this.lstSuggestions_SelectedIndexChanged);
            this.lstSuggestions.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstSuggestions_KeyPress);
            // 
            // Form_Customer_Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(512, 512);
            this.Controls.Add(this.spltcntrSearch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(512, 512);
            this.MinimumSize = new System.Drawing.Size(512, 28);
            this.Name = "Form_Customer_Search";
            this.Load += new System.EventHandler(this.Form_Customer_Search_Load);
            this.spltcntrSearch.Panel1.ResumeLayout(false);
            this.spltcntrSearch.Panel1.PerformLayout();
            this.spltcntrSearch.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltcntrSearch)).EndInit();
            this.spltcntrSearch.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtQuery;
        private System.Windows.Forms.SplitContainer spltcntrSearch;
        private System.Windows.Forms.ListBox lstSuggestions;
    }
}