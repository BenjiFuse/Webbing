﻿namespace Webbing
{
    partial class Form_Customers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLogin = new System.Windows.Forms.Label();
            this.mnuCustomers = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel_CustomersMenu = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnAddNewCustomer = new System.Windows.Forms.Button();
            this.btnEditCustomer = new System.Windows.Forms.Button();
            this.btnAddRelative = new System.Windows.Forms.Button();
            this.dgvCustomers = new System.Windows.Forms.DataGridView();
            this.columnCID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnFirst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnMiddle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnLast = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnDoB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnWID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnAddress1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnAddress2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnZipcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnCellphone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnHomephone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnWorkphone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnEmergencyContact = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnEmergencyContactPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnCertification = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnCertifiedBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mnuCustomers.SuspendLayout();
            this.panel_CustomersMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomers)).BeginInit();
            this.SuspendLayout();
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.BackColor = System.Drawing.Color.Teal;
            this.lblLogin.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogin.ForeColor = System.Drawing.Color.Coral;
            this.lblLogin.Location = new System.Drawing.Point(3, 50);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(75, 15);
            this.lblLogin.TabIndex = 0;
            this.lblLogin.Text = "Logged in as";
            // 
            // mnuCustomers
            // 
            this.mnuCustomers.BackColor = System.Drawing.Color.Coral;
            this.mnuCustomers.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.mnuCustomers.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mnuCustomers.Location = new System.Drawing.Point(0, 74);
            this.mnuCustomers.Name = "mnuCustomers";
            this.mnuCustomers.Size = new System.Drawing.Size(1360, 24);
            this.mnuCustomers.TabIndex = 2;
            this.mnuCustomers.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // panel_CustomersMenu
            // 
            this.panel_CustomersMenu.BackColor = System.Drawing.Color.Teal;
            this.panel_CustomersMenu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_CustomersMenu.Controls.Add(this.mnuCustomers);
            this.panel_CustomersMenu.Controls.Add(this.lblLogin);
            this.panel_CustomersMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_CustomersMenu.Location = new System.Drawing.Point(0, 0);
            this.panel_CustomersMenu.Name = "panel_CustomersMenu";
            this.panel_CustomersMenu.Size = new System.Drawing.Size(1362, 100);
            this.panel_CustomersMenu.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 641);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1362, 100);
            this.panel1.TabIndex = 5;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.btnRefresh, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnAddNewCustomer, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnEditCustomer, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnAddRelative, 3, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1031, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(331, 100);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnRefresh.Location = new System.Drawing.Point(0, 0);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(0);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(82, 50);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "REFRESH";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnAddNewCustomer
            // 
            this.btnAddNewCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAddNewCustomer.Location = new System.Drawing.Point(246, 0);
            this.btnAddNewCustomer.Margin = new System.Windows.Forms.Padding(0);
            this.btnAddNewCustomer.Name = "btnAddNewCustomer";
            this.btnAddNewCustomer.Size = new System.Drawing.Size(85, 50);
            this.btnAddNewCustomer.TabIndex = 0;
            this.btnAddNewCustomer.Text = "Add New Customer";
            this.btnAddNewCustomer.UseVisualStyleBackColor = true;
            this.btnAddNewCustomer.Click += new System.EventHandler(this.btnAddNewCustomer_Click);
            // 
            // btnEditCustomer
            // 
            this.btnEditCustomer.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnEditCustomer.Location = new System.Drawing.Point(164, 0);
            this.btnEditCustomer.Margin = new System.Windows.Forms.Padding(0);
            this.btnEditCustomer.Name = "btnEditCustomer";
            this.btnEditCustomer.Size = new System.Drawing.Size(82, 50);
            this.btnEditCustomer.TabIndex = 1;
            this.btnEditCustomer.Text = "Edit Customer";
            this.btnEditCustomer.UseVisualStyleBackColor = true;
            this.btnEditCustomer.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAddRelative
            // 
            this.btnAddRelative.Location = new System.Drawing.Point(246, 50);
            this.btnAddRelative.Margin = new System.Windows.Forms.Padding(0);
            this.btnAddRelative.Name = "btnAddRelative";
            this.btnAddRelative.Size = new System.Drawing.Size(85, 50);
            this.btnAddRelative.TabIndex = 2;
            this.btnAddRelative.Text = "Add Relative";
            this.btnAddRelative.UseVisualStyleBackColor = true;
            // 
            // dgvCustomers
            // 
            this.dgvCustomers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustomers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnCID,
            this.columnFirst,
            this.columnMiddle,
            this.columnLast,
            this.columnDoB,
            this.columnWID,
            this.columnAddress1,
            this.columnAddress2,
            this.columnZipcode,
            this.columnCity,
            this.columnState,
            this.columnCountry,
            this.columnCellphone,
            this.columnHomephone,
            this.columnWorkphone,
            this.columnEmail,
            this.columnEmergencyContact,
            this.columnEmergencyContactPhone,
            this.columnCertification,
            this.columnCertifiedBy});
            this.dgvCustomers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCustomers.Location = new System.Drawing.Point(0, 100);
            this.dgvCustomers.Name = "dgvCustomers";
            this.dgvCustomers.Size = new System.Drawing.Size(1362, 541);
            this.dgvCustomers.TabIndex = 6;
            // 
            // columnCID
            // 
            this.columnCID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.columnCID.HeaderText = "CID";
            this.columnCID.Name = "columnCID";
            this.columnCID.Width = 50;
            // 
            // columnFirst
            // 
            this.columnFirst.HeaderText = "First";
            this.columnFirst.Name = "columnFirst";
            // 
            // columnMiddle
            // 
            this.columnMiddle.HeaderText = "Middle";
            this.columnMiddle.Name = "columnMiddle";
            // 
            // columnLast
            // 
            this.columnLast.HeaderText = "Last";
            this.columnLast.Name = "columnLast";
            // 
            // columnDoB
            // 
            this.columnDoB.HeaderText = "DoB";
            this.columnDoB.Name = "columnDoB";
            // 
            // columnWID
            // 
            this.columnWID.HeaderText = "WID";
            this.columnWID.Name = "columnWID";
            // 
            // columnAddress1
            // 
            this.columnAddress1.HeaderText = "Address 1";
            this.columnAddress1.Name = "columnAddress1";
            // 
            // columnAddress2
            // 
            this.columnAddress2.HeaderText = "Address 2";
            this.columnAddress2.Name = "columnAddress2";
            // 
            // columnZipcode
            // 
            this.columnZipcode.HeaderText = "ZIP Code";
            this.columnZipcode.Name = "columnZipcode";
            // 
            // columnCity
            // 
            this.columnCity.HeaderText = "City";
            this.columnCity.Name = "columnCity";
            // 
            // columnState
            // 
            this.columnState.HeaderText = "State";
            this.columnState.Name = "columnState";
            // 
            // columnCountry
            // 
            this.columnCountry.HeaderText = "Country";
            this.columnCountry.Name = "columnCountry";
            // 
            // columnCellphone
            // 
            this.columnCellphone.HeaderText = "Cellphone";
            this.columnCellphone.Name = "columnCellphone";
            // 
            // columnHomephone
            // 
            this.columnHomephone.HeaderText = "Home Phone";
            this.columnHomephone.Name = "columnHomephone";
            // 
            // columnWorkphone
            // 
            this.columnWorkphone.HeaderText = "Work Phone";
            this.columnWorkphone.Name = "columnWorkphone";
            // 
            // columnEmail
            // 
            this.columnEmail.HeaderText = "Email";
            this.columnEmail.Name = "columnEmail";
            // 
            // columnEmergencyContact
            // 
            this.columnEmergencyContact.HeaderText = "Emergency Contact";
            this.columnEmergencyContact.Name = "columnEmergencyContact";
            // 
            // columnEmergencyContactPhone
            // 
            this.columnEmergencyContactPhone.HeaderText = "Emergency Contact Phone";
            this.columnEmergencyContactPhone.Name = "columnEmergencyContactPhone";
            // 
            // columnCertification
            // 
            this.columnCertification.HeaderText = "Certification";
            this.columnCertification.Name = "columnCertification";
            // 
            // columnCertifiedBy
            // 
            this.columnCertifiedBy.HeaderText = "Certified By";
            this.columnCertifiedBy.Name = "columnCertifiedBy";
            // 
            // Form_Customers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.Controls.Add(this.dgvCustomers);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel_CustomersMenu);
            this.MainMenuStrip = this.mnuCustomers;
            this.Name = "Form_Customers";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Webbing - Customers";
            this.Load += new System.EventHandler(this.Form_Customers_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form_Customers_KeyDown);
            this.mnuCustomers.ResumeLayout(false);
            this.mnuCustomers.PerformLayout();
            this.panel_CustomersMenu.ResumeLayout(false);
            this.panel_CustomersMenu.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.MenuStrip mnuCustomers;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Panel panel_CustomersMenu;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAddNewCustomer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnEditCustomer;
        private System.Windows.Forms.Button btnAddRelative;
        private System.Windows.Forms.DataGridView dgvCustomers;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnCID;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnFirst;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnMiddle;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnLast;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnDoB;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnWID;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnAddress1;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnAddress2;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnZipcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnState;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnCellphone;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnHomephone;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnWorkphone;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnEmergencyContact;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnEmergencyContactPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnCertification;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnCertifiedBy;
    }
}