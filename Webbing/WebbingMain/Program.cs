﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Webbing
{
    // Main Application thread
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form_Customers());
        }

    }

    // Holds various utility functions for data validation
    public static class Utilities
    {
          // Returns only the characters from the input string that are valid for DB fields
        public static string SanitizeString(string input)
        {
            return new String(input.Where(IsValidCharacter).ToArray());
        }

        // Returns true if the provided char is a valid input for DB fields
        public static bool IsValidCharacter(char c)
        {
            try
            {
                bool result = false;
                if (Char.IsLetterOrDigit(c))
                {
                    result = true;
                } else if (Char.IsPunctuation(c))
                {
                    result = true;
                }
                return result;
            } catch (ArgumentException e)
            {
                MessageBox.Show("Error: Utilities.IsValidCharacter on character=\n\n" + e.Message);
                return false;
            }

        }

        // Returns a Utilities.Date represenating the passed string date (MM/DD/YYYY format)
        public static DateTime? DateFromString(string input)
        {
            if (input == null || input == "")
            {
                return null;
            }
            try
            {
                List<int> components = new List<int>();
                foreach (String component in input.Split('/')) {
                    components.Add(int.Parse(component));
                }
                if (components.Count != 3)
                {
                    throw new ArgumentException("Error: Invalid date of birth entered.");
                }
                DateTime date = new DateTime(components[1], components[0], components[2]);
                return date;
            } catch (Exception e)       // [!] Very bad and dumb, replace with sensible fault handling (or remove)
            {
                MessageBox.Show("ERROR: Utilities.DateFromString\n" + e.Message);
                return null;
            }
        }

        public static bool IsDateBeforeToday(DateTime date)
        {
            return (date < DateTime.Today) ? true : false;
        }

        public static bool IsDateAfterToday(DateTime date)
        {
            return (date > DateTime.Today) ? true : false;
        }

    }

    public static class Exceptions
    {
        
    }

}
