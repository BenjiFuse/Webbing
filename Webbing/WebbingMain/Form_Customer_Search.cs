﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Webbing
{
    public partial class Form_Customer_Search : Form
    {
        Form opener;

        // Default constructor accepts reference to opener form for passing query back
        public Form_Customer_Search(Form opener, char firstchar)
        {
            InitializeComponent();
            this.opener = opener;
            txtQuery.Text += firstchar;                         // Put opening keypress into query
            txtQuery_SelectLastWord();
        }

        private void txtQuery_SelectLastWord()
        {
            txtQuery.Select();
            txtQuery.SelectionStart = txtQuery.Text.Length;     // Put cursor after word to prevent select-all
            txtQuery.SelectionLength = 0;
        }

        private void Form_Customer_Search_Load(object sender, EventArgs e)
        {
            Point newloc = new Point();
            newloc.X = opener.Location.X + opener.Size.Width/2 - this.Size.Width/2;
            newloc.Y = opener.Location.Y + opener.Size.Height/2 - this.Size.Height/2;
            this.Location = newloc;
            
        }

        private void txtQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();       // Close search prompt on escape key
            }
            else if (e.KeyCode == Keys.Enter)
            {
                String query = txtQuery.Text;
                ((Form_Customers)opener).CustomerSearch(query);
                this.Close();
            }
            else if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Down)
            {
                //lstSuggestions.Focus();
                lstSuggestions.SelectedItem = 0 ;
            } else if (e.KeyCode == Keys.Up) {
                lstSuggestions.Focus();
                lstSuggestions.SelectedItem = lstSuggestions.Items.Count-1;
            }
          

        }

        private void txtQuery_TextChanged(object sender, EventArgs e)
        {
            this.lstSuggestions.Items.Clear();
            string[] suggestions = { "first", "last", "address", "status" };

            this.lstSuggestions.Items.AddRange(suggestions);

            // this.lstSuggestions.Height = 16 * suggestions.Count(); [!] [?] Find a better way to modulate suggestion box size
        }

        private void lstSuggestions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstSuggestions.SelectedIndex >= 0)
            {
                //txtQuery.Text = lstSuggestions.SelectedItem.ToString();
            }
            
        }

        // Eventhandler for keypress to process suggestion navigation
        private void lstSuggestions_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                this.txtQuery.Select();             // Select txtQuery box when esc pressed from suggestions
            }
            else if (e.KeyChar == (char)Keys.Enter)
            {
                if (lstSuggestions.Items.Count >= 0)
                {
                    txtQuery.Text = lstSuggestions.SelectedItem.ToString();         // Put suggestion into query box
                    this.txtQuery.Select();             // Select the query field when a selection is made with enter key
                }
            }
            else if (e.KeyChar == (char)Keys.Up)
            {
                if (lstSuggestions.SelectedIndex <= 0)
                {
                    this.lstSuggestions.SelectedIndex = this.lstSuggestions.Items.Count - 1;
                }
                else {
                    this.lstSuggestions.SelectedIndex--;
                }
            }
            else if (e.KeyChar == (char)Keys.Down)
            {
                if (lstSuggestions.SelectedIndex >= lstSuggestions.Items.Count - 1)
                {
                    this.lstSuggestions.SelectedIndex = 0;
                }
                else {
                    this.lstSuggestions.SelectedIndex++;
                }
            }

        }

    }
}