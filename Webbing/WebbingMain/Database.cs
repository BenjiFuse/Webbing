﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;     // [?] only here to print db exception messsages...
using MySql.Data;
using MySql.Data.MySqlClient;

/*
 * Database.cs handles operating on and querying the accompanying MySQL DB
 * Exposes stored procedures in the DB for CRUD operations.
 */
namespace Webbing
{
    class Database
    {
        public string server = "SERVER=127.0.0.1;";
        public string port = "PORT=3307;";
        public string database = "DATABASE=webbing";
        public string uid = "UID=webbing;";
        public string pwd = "PWD=bouldering";
        public string connectionString;
        private static Database _instance = null;
        public MySqlConnection connection;


        // Singleton instancing of Database Connection object
        public static Database Instance()
        {
            if (_instance == null)
            {
                _instance = new Database();
            }
            return _instance;
        }
        
        // Returns the status of the DB Connection, starting a new one if none already
        public bool IsConnected()
        {
            bool result = true;
            if (connection == null || connection.State == ConnectionState.Closed)   // [?] [!] Might need additional checks for error or exec conn states
            {
                try
                {
                    connectionString = server + port + uid + pwd + database;
                    connection = new MySqlConnection(connectionString);
                    connection.Open();
                    result = true;
                }
                catch (MySql.Data.MySqlClient.MySqlException e)
                {
                    MessageBox.Show(e.Number + "\n" + e.Message); // [!] Need more graceful exception handling
                    result = false;
                }
            }

            return result;
        }

        // Calls the stored procedure add_new_customer on the DB
        public bool add_new_customer(string first, string last, string middle, string email, string address1,
            string address2, string city, string state, string country, string cellphone, string homephone,
            string certification, string dob)
        {
            if (!IsConnected())
            {
                MessageBox.Show("ERROR: No DB Connection present!");
                // [!] Need more graceful error handling here
                return false;
            }
            try
            {
                MySqlCommand cmd = new MySqlCommand("webbing.add_new_customer", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@arg_first", first);
                cmd.Parameters.AddWithValue("@arg_last", last);
                cmd.Parameters.AddWithValue("@arg_middle", middle);
                cmd.Parameters.AddWithValue("@arg_email", email);
                cmd.Parameters.AddWithValue("@arg_address1", address1);
                cmd.Parameters.AddWithValue("@arg_address2", address2);
                cmd.Parameters.AddWithValue("@arg_city", city);
                cmd.Parameters.AddWithValue("@arg_state", state);
                cmd.Parameters.AddWithValue("@arg_country", country);
                cmd.Parameters.AddWithValue("@arg_cellphone", cellphone);
                cmd.Parameters.AddWithValue("@arg_homephone", homephone);
                cmd.Parameters.AddWithValue("@arg_certification", certification);
                cmd.Parameters.AddWithValue("@arg_dob", dob);

                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Console.WriteLine(rdr[0] + " --- " + rdr[1]);
                }
                rdr.Close();
            }
            catch (MySql.Data.MySqlClient.MySqlException e)
            {
                MessageBox.Show(e.Message);        // [!] Need more graceful exception handling
                return false;
            }
            catch (System.InvalidOperationException e)
            {
                MessageBox.Show(e.Message);         // [!] Need more graceful exception handling
                return false;
            }
            catch (System.ArgumentException e)
            {
                MessageBox.Show(e.Message);         // [!] Need more graceful exception handling
                return false;
            }

            connection.Close();
            return true;
        }

        // Calls the stored procedure update_existing_customer on the connected DB
        // 
        public bool update_existing_customer(long cid, string first, string last, string middle, string email, string address1,
            string address2, string city, string state, string country, string cellphone, string homephone, string certification, string dob)
        {
            if (!IsConnected())
            {
                MessageBox.Show("ERROR: No DB Connection present!");
                // [!] Need more graceful error handling here
                return false;
            }
            try
            {
                MySqlCommand cmd = new MySqlCommand("webbing.add_new_customer", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@arg_cid", cid);
                cmd.Parameters.AddWithValue("@arg_first", first);
                cmd.Parameters.AddWithValue("@arg_last", last);
                cmd.Parameters.AddWithValue("@arg_middle", middle);
                cmd.Parameters.AddWithValue("@arg_email", email);
                cmd.Parameters.AddWithValue("@arg_address1", address1);
                cmd.Parameters.AddWithValue("@arg_address2", address2);
                cmd.Parameters.AddWithValue("@arg_city", city);
                cmd.Parameters.AddWithValue("@arg_state", state);
                cmd.Parameters.AddWithValue("@arg_country", country);
                cmd.Parameters.AddWithValue("@arg_cellphone", cellphone);
                cmd.Parameters.AddWithValue("@arg_homephone", homephone);
                cmd.Parameters.AddWithValue("@arg_certification", certification);
                cmd.Parameters.AddWithValue("@arg_dob", dob);

                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Console.WriteLine(rdr[0] + " --- " + rdr[1]);
                }
                rdr.Close();
            }
            catch (MySql.Data.MySqlClient.MySqlException e)
            {
                MessageBox.Show(e.Message);        // [!] Need more graceful exception handling
                return false;
            }
            catch (System.InvalidOperationException e)
            {
                MessageBox.Show(e.Message);         // [!] Need more graceful exception handling
                return false;
            }
            catch (System.ArgumentException e)
            {
                MessageBox.Show(e.Message);         // [!] Need more graceful exception handling
                return false;
            }
            connection.Close();
            return true;
        }

        // Executes a selected stored procedure to query the customers table and return the desired elements
        public bool customers_query(long cid = 0, string first = "", string last = "", string middle = "", string email = "", string address1 = "",
            string address2 = "", string zipcode = "", string city = "", string state = "", string country = "",
            string cellphone = "", string homephone = "", string workphone = "", string certification = "", string certifiedby = "",
            string emergencycontact = "", string emergencycontactphone = "", DateTime? dob = null)
        {
            // Build the query string:
            StringBuilder query = new StringBuilder();
            query.Append("SELECT * FROM webbing.customers WHERE ");
            if (cid > 0) { query.Append("cid=" + cid + " AND "); }
            if (first != "") { query.Append("first=" + first + " AND "); }
            if (last != "") { query.Append("last=" + last + " AND "); }
            if (middle != "") { query.Append("middle=" + middle + " AND "); }
            if (address1 != "") { query.Append("address1=" + address1 + " AND "); }
            if (address2 != "") { query.Append("address2=" + address2 + " AND "); }
            if (zipcode != "") { query.Append("zipcode=" + zipcode + " AND "); }
            if (city != "") { query.Append("city=" + city + " AND "); }
            if (state != "") { query.Append("state=" + state + " AND "); }
            if (country != "") { query.Append("country=" + country + " AND "); }
            if (cellphone != "") { query.Append("cellphone=" + cellphone + " AND "); }
            if (homephone != "") { query.Append("homephone=" + homephone + " AND "); }
            if (workphone != "") { query.Append("workphone=" + workphone + " AND "); }
            if (email != "") { query.Append("email=" + email + " AND "); }
            if (emergencycontact != "") { query.Append("emergencycontact=" + emergencycontact + " AND "); }
            if (emergencycontactphone != "") { query.Append("emergencycontactphone=" + emergencycontactphone + " AND "); }
            if (certification != "") { query.Append("certification=" + certification + " AND "); }
            if (certifiedby != "") { query.Append("certifiedby=" + certifiedby + " AND "); }
            if (dob != null) { query.Append("dob=" + dob.Value.Date.ToString()); }  // [?] Might need to convert dob to text different

            string result = query.ToString();
            if (result.Substring(result.Length - 5, 5).CompareTo(" AND ") == 0)
            {
                result = result.Remove(result.Length - 5, 5);  // remove trailing " AND " sequence
            }
            result = result + ";";                    // add delimiter
            if (result.Length <= 38)
            {
                MessageBox.Show("Error: Queries require at least one field to search for.");
                return false;
            }
            MessageBox.Show(result);    // [!] DEBUG
            if (!IsConnected())
            {
                MessageBox.Show("ERROR: No DB Connection present!");
                // [!] Need more graceful error handling here
                return false;
            }
            try
            {
                MySqlCommand cmd = new MySqlCommand("webbing.add_new_customer", connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = result;
                MySqlDataReader rdr = cmd.ExecuteReader();
                Customer.clear_customer_list();
                
                while (rdr.Read())
                {
                    new Customer(int.Parse(rdr.GetString(0)), int.Parse(rdr.GetString(1)), rdr.GetString(2), rdr.GetString(3),
                    rdr.GetString(4), rdr.GetString(5), rdr.GetString(6), rdr.GetString(7), rdr.GetString(8), rdr.GetString(9),
                    rdr.GetString(10), rdr.GetString(11), rdr.GetString(12), rdr.GetString(13), rdr.GetString(14),
                    rdr.GetString(15), rdr.GetString(16), rdr.GetString(17), rdr.GetString(18), dob: null);
                }
                rdr.Close();
            }
            catch (MySql.Data.MySqlClient.MySqlException e)
            {
                MessageBox.Show(e.Message);        // [!] Need more graceful exception handling
                return false;
            }
            catch (System.InvalidOperationException e)
            {
                MessageBox.Show(e.Message);         // [!] Need more graceful exception handling
                return false;
            }
            catch (System.ArgumentException e)
            {
                MessageBox.Show(e.Message);         // [!] Need more graceful exception handling
                return false;
            }
            connection.Close();
            return true;
        }
    }
}
