﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Webbing
{
    public partial class Form_Update_Customer : Form
    {
        private Dictionary<string, Control> info_to_control;    // Dict to store relation between info categories and control elements
        private Dictionary<string, Control> info_to_label;      // Dict to store relation between info categories and label elements

        public Form_Update_Customer(Customer customer)
        {
            InitializeComponent();
            info_to_control = new Dictionary<string, Control>();
            info_to_control.Add("first", txtFirst);
            info_to_control.Add("last", txtLast);
            info_to_control.Add("middle", txtMiddle);
            info_to_control.Add("dob", txtDoB);
            info_to_control.Add("address1", txtAddress1);
            info_to_control.Add("address2", txtAddress2);
            info_to_control.Add("zipcode", txtZipcode);
            info_to_control.Add("city", txtCity);
            info_to_control.Add("state", cboState);
            info_to_control.Add("country", txtCountry);
            info_to_control.Add("cellphone", txtCellPhone);
            info_to_control.Add("homephone", txtHomePhone);
            info_to_control.Add("workphone", txtWorkPhone);
            info_to_control.Add("email", txtEmailAddress);
            info_to_control.Add("emergencycontact", txtEmergencyContact);
            info_to_control.Add("emergencycontactphone", txtEmergencyContactPhone);
            info_to_control.Add("certification", cboLevel);
            info_to_control.Add("certifiedby", cboCertifiedBy);

            info_to_label = new Dictionary<string, Control>();
            info_to_label.Add("first", lblFirst);
            info_to_label.Add("last", lblLast);
            info_to_label.Add("middle", lblMiddle);
            info_to_label.Add("dob", lblDoB);
            info_to_label.Add("address1", lblAddress1);
            info_to_label.Add("address2", lblAddress2);
            info_to_label.Add("zipcode", lblZipcode);
            info_to_label.Add("city", lblCity);
            info_to_label.Add("state", lblState);
            info_to_label.Add("country", lblCountry);
            info_to_label.Add("cellphone", lblCellPhone);
            info_to_label.Add("homephone", lblHomePhone);
            info_to_label.Add("workphone", lblWorkPhone);
            info_to_label.Add("email", lblEmailAddress);
            info_to_label.Add("emergencycontact", lblEmergencyContact);
            info_to_label.Add("emergencycontactphone", lblEmergencyContactPhone);
            info_to_label.Add("certification", lblLevel);
            info_to_label.Add("certifiedby", lblCertifiedBy);
        }

        // Finds the label and input controls for the provided string category and highlights them (red text)
        private void highlightInvalidEntry(string category)
        {
            Control control;
            if (!info_to_control.TryGetValue(category, out control)) {
                throw new ArgumentException("ERROR: Form_Update_Customer.highlightInvalidEntry\n");
            }
            control.ForeColor = System.Drawing.Color.Red;

            if (!info_to_label.TryGetValue(category, out control))
            {
                throw new ArgumentException("ERROR: Form_Update_Customer.highlightInvalidEntry\n");
            }
            control.ForeColor = System.Drawing.Color.Red;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> customer_information = new Dictionary<string, string>();
            customer_information.Add("first", Utilities.SanitizeString(txtFirst.Text));
            customer_information.Add("middle", Utilities.SanitizeString(txtMiddle.Text));
            customer_information.Add("last", Utilities.SanitizeString(txtLast.Text));
            customer_information.Add("dob", Utilities.SanitizeString(txtDoB.Text));
            customer_information.Add("address1", Utilities.SanitizeString(txtAddress1.Text));
            customer_information.Add("address2", Utilities.SanitizeString(txtAddress2.Text));
            customer_information.Add("zipcode", Utilities.SanitizeString(txtZipcode.Text));
            customer_information.Add("city", Utilities.SanitizeString(txtCity.Text));
            customer_information.Add("state", Utilities.SanitizeString(cboState.Text));
            customer_information.Add("country", Utilities.SanitizeString(txtCountry.Text));
            customer_information.Add("cellphone", Utilities.SanitizeString(txtCellPhone.Text));
            customer_information.Add("homephone", Utilities.SanitizeString(txtHomePhone.Text));
            customer_information.Add("workphone", Utilities.SanitizeString(txtWorkPhone.Text));
            customer_information.Add("email", Utilities.SanitizeString(txtEmailAddress.Text));
            customer_information.Add("emergencycontact", Utilities.SanitizeString(txtEmergencyContact.Text));
            customer_information.Add("emergencycontactphone", Utilities.SanitizeString(txtEmergencyContactPhone.Text));
            customer_information.Add("certification", Utilities.SanitizeString(cboLevel.Text));
            customer_information.Add("certifiedby", Utilities.SanitizeString(cboCertifiedBy.Text));
            DateTime? dob = Utilities.DateFromString(Utilities.SanitizeString(txtDoB.Text));

            // Validate each provided field
            bool valid = false;
            for (int i = 0; i < customer_information.Count; i++)
            {
                if (customer_information.ElementAt(i).Value == "")
                {
                    highlightInvalidEntry(customer_information.ElementAt(i).Key);   // Highlight invalid fields
                }
                else if (dob == null)
                {
                    highlightInvalidEntry(customer_information.ElementAt(i).Key);   // Highlight invalid fields
                }
                else if (DateTime.Today <= dob)
                {
                    MessageBox.Show("This customer may be a time traveler, proceed with caution.");
                    highlightInvalidEntry(customer_information.ElementAt(i).Key);   // Highlight invalid fields
                } else
                {
                    valid = true;
                }
            }

            if (valid)
            {
                // [!] TODO: Update/Create new customer in DB
            } else
            {
                MessageBox.Show("Invalid inputs detected.");
            }
        }
    }
}
