﻿namespace Webbing
{
    partial class Form_Update_Customer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpPersonalInformation = new System.Windows.Forms.GroupBox();
            this.lblDoB = new System.Windows.Forms.Label();
            this.txtLast = new System.Windows.Forms.TextBox();
            this.lblLast = new System.Windows.Forms.Label();
            this.txtMiddle = new System.Windows.Forms.TextBox();
            this.lblMiddle = new System.Windows.Forms.Label();
            this.txtFirst = new System.Windows.Forms.TextBox();
            this.lblFirst = new System.Windows.Forms.Label();
            this.grpCertifications = new System.Windows.Forms.GroupBox();
            this.lblCertifiedBy = new System.Windows.Forms.Label();
            this.cboLevel = new System.Windows.Forms.ComboBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.lblAddress1 = new System.Windows.Forms.Label();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.lblAddress2 = new System.Windows.Forms.Label();
            this.lblZipcode = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtZipcode = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.lblCountry = new System.Windows.Forms.Label();
            this.btnCopyAddressFromRelative = new System.Windows.Forms.Button();
            this.grpAddress = new System.Windows.Forms.GroupBox();
            this.grpContactInfo = new System.Windows.Forms.GroupBox();
            this.txtWorkPhone = new System.Windows.Forms.TextBox();
            this.lblWorkPhone = new System.Windows.Forms.Label();
            this.btnCopyContactFromRelative = new System.Windows.Forms.Button();
            this.txtCellPhone = new System.Windows.Forms.TextBox();
            this.lblHomePhone = new System.Windows.Forms.Label();
            this.txtHomePhone = new System.Windows.Forms.TextBox();
            this.lblCellPhone = new System.Windows.Forms.Label();
            this.txtEmergencyContactPhone = new System.Windows.Forms.TextBox();
            this.lblEmergencyContact = new System.Windows.Forms.Label();
            this.lblEmergencyContactPhone = new System.Windows.Forms.Label();
            this.txtEmergencyContact = new System.Windows.Forms.TextBox();
            this.lblLevel = new System.Windows.Forms.Label();
            this.cboState = new System.Windows.Forms.ComboBox();
            this.txtCountry = new System.Windows.Forms.TextBox();
            this.cboCertifiedBy = new System.Windows.Forms.ComboBox();
            this.txtDoB = new System.Windows.Forms.TextBox();
            this.txtEmailAddress = new System.Windows.Forms.TextBox();
            this.lblEmailAddress = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabInformation = new System.Windows.Forms.TabPage();
            this.tabMembership = new System.Windows.Forms.TabPage();
            this.tabStatus = new System.Windows.Forms.TabPage();
            this.tabNotes = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.grpPersonalInformation.SuspendLayout();
            this.grpCertifications.SuspendLayout();
            this.grpAddress.SuspendLayout();
            this.grpContactInfo.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabInformation.SuspendLayout();
            this.tabStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // grpPersonalInformation
            // 
            this.grpPersonalInformation.Controls.Add(this.lblDoB);
            this.grpPersonalInformation.Controls.Add(this.txtDoB);
            this.grpPersonalInformation.Controls.Add(this.txtLast);
            this.grpPersonalInformation.Controls.Add(this.lblLast);
            this.grpPersonalInformation.Controls.Add(this.txtMiddle);
            this.grpPersonalInformation.Controls.Add(this.lblMiddle);
            this.grpPersonalInformation.Controls.Add(this.txtFirst);
            this.grpPersonalInformation.Controls.Add(this.lblFirst);
            this.grpPersonalInformation.Location = new System.Drawing.Point(6, 6);
            this.grpPersonalInformation.Name = "grpPersonalInformation";
            this.grpPersonalInformation.Size = new System.Drawing.Size(571, 82);
            this.grpPersonalInformation.TabIndex = 0;
            this.grpPersonalInformation.TabStop = false;
            this.grpPersonalInformation.Text = "Personal Information";
            // 
            // lblDoB
            // 
            this.lblDoB.AutoSize = true;
            this.lblDoB.Location = new System.Drawing.Point(9, 51);
            this.lblDoB.Name = "lblDoB";
            this.lblDoB.Size = new System.Drawing.Size(147, 13);
            this.lblDoB.TabIndex = 4;
            this.lblDoB.Text = "Date of Birth (MM/DD/YYYY)";
            // 
            // txtLast
            // 
            this.txtLast.Location = new System.Drawing.Point(447, 19);
            this.txtLast.Name = "txtLast";
            this.txtLast.Size = new System.Drawing.Size(110, 20);
            this.txtLast.TabIndex = 3;
            this.txtLast.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblLast
            // 
            this.lblLast.AutoSize = true;
            this.lblLast.Location = new System.Drawing.Point(383, 22);
            this.lblLast.Name = "lblLast";
            this.lblLast.Size = new System.Drawing.Size(58, 13);
            this.lblLast.TabIndex = 3;
            this.lblLast.Text = "Last Name";
            // 
            // txtMiddle
            // 
            this.txtMiddle.Location = new System.Drawing.Point(261, 19);
            this.txtMiddle.Name = "txtMiddle";
            this.txtMiddle.Size = new System.Drawing.Size(110, 20);
            this.txtMiddle.TabIndex = 2;
            this.txtMiddle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblMiddle
            // 
            this.lblMiddle.AutoSize = true;
            this.lblMiddle.Location = new System.Drawing.Point(188, 22);
            this.lblMiddle.Name = "lblMiddle";
            this.lblMiddle.Size = new System.Drawing.Size(69, 13);
            this.lblMiddle.TabIndex = 2;
            this.lblMiddle.Text = "Middle Name";
            // 
            // txtFirst
            // 
            this.txtFirst.Location = new System.Drawing.Point(72, 19);
            this.txtFirst.Name = "txtFirst";
            this.txtFirst.Size = new System.Drawing.Size(110, 20);
            this.txtFirst.TabIndex = 1;
            this.txtFirst.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFirst.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblFirst
            // 
            this.lblFirst.AutoSize = true;
            this.lblFirst.Location = new System.Drawing.Point(9, 22);
            this.lblFirst.Name = "lblFirst";
            this.lblFirst.Size = new System.Drawing.Size(57, 13);
            this.lblFirst.TabIndex = 1;
            this.lblFirst.Text = "First Name";
            // 
            // grpCertifications
            // 
            this.grpCertifications.Controls.Add(this.lblLevel);
            this.grpCertifications.Controls.Add(this.lblCertifiedBy);
            this.grpCertifications.Controls.Add(this.cboCertifiedBy);
            this.grpCertifications.Controls.Add(this.cboLevel);
            this.grpCertifications.Location = new System.Drawing.Point(6, 355);
            this.grpCertifications.Name = "grpCertifications";
            this.grpCertifications.Size = new System.Drawing.Size(450, 53);
            this.grpCertifications.TabIndex = 3;
            this.grpCertifications.TabStop = false;
            this.grpCertifications.Text = "Certifications";
            // 
            // lblCertifiedBy
            // 
            this.lblCertifiedBy.AutoSize = true;
            this.lblCertifiedBy.Location = new System.Drawing.Point(218, 23);
            this.lblCertifiedBy.Name = "lblCertifiedBy";
            this.lblCertifiedBy.Size = new System.Drawing.Size(60, 13);
            this.lblCertifiedBy.TabIndex = 18;
            this.lblCertifiedBy.Text = "Certified By";
            this.lblCertifiedBy.Click += new System.EventHandler(this.label13_Click);
            // 
            // cboLevel
            // 
            this.cboLevel.FormattingEnabled = true;
            this.cboLevel.Location = new System.Drawing.Point(48, 20);
            this.cboLevel.Name = "cboLevel";
            this.cboLevel.Size = new System.Drawing.Size(155, 21);
            this.cboLevel.TabIndex = 17;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(463, 355);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(115, 53);
            this.btnSubmit.TabIndex = 4;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(71, 22);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(299, 20);
            this.txtAddress1.TabIndex = 5;
            this.txtAddress1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblAddress1
            // 
            this.lblAddress1.AutoSize = true;
            this.lblAddress1.Location = new System.Drawing.Point(9, 25);
            this.lblAddress1.Name = "lblAddress1";
            this.lblAddress1.Size = new System.Drawing.Size(54, 13);
            this.lblAddress1.TabIndex = 5;
            this.lblAddress1.Text = "Address 1";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(71, 49);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(299, 20);
            this.txtAddress2.TabIndex = 6;
            this.txtAddress2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblAddress2
            // 
            this.lblAddress2.AutoSize = true;
            this.lblAddress2.Location = new System.Drawing.Point(9, 52);
            this.lblAddress2.Name = "lblAddress2";
            this.lblAddress2.Size = new System.Drawing.Size(54, 13);
            this.lblAddress2.TabIndex = 6;
            this.lblAddress2.Text = "Address 2";
            // 
            // lblZipcode
            // 
            this.lblZipcode.AutoSize = true;
            this.lblZipcode.Location = new System.Drawing.Point(9, 79);
            this.lblZipcode.Name = "lblZipcode";
            this.lblZipcode.Size = new System.Drawing.Size(52, 13);
            this.lblZipcode.TabIndex = 7;
            this.lblZipcode.Text = "ZIP Code";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(168, 76);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(133, 20);
            this.txtCity.TabIndex = 8;
            this.txtCity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(138, 79);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(24, 13);
            this.lblCity.TabIndex = 8;
            this.lblCity.Text = "City";
            // 
            // txtZipcode
            // 
            this.txtZipcode.Location = new System.Drawing.Point(71, 76);
            this.txtZipcode.Name = "txtZipcode";
            this.txtZipcode.Size = new System.Drawing.Size(59, 20);
            this.txtZipcode.TabIndex = 7;
            this.txtZipcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(311, 79);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(32, 13);
            this.lblState.TabIndex = 9;
            this.lblState.Text = "State";
            // 
            // lblCountry
            // 
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(408, 79);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(43, 13);
            this.lblCountry.TabIndex = 10;
            this.lblCountry.Text = "Country";
            // 
            // btnCopyAddressFromRelative
            // 
            this.btnCopyAddressFromRelative.Location = new System.Drawing.Point(379, 19);
            this.btnCopyAddressFromRelative.Name = "btnCopyAddressFromRelative";
            this.btnCopyAddressFromRelative.Size = new System.Drawing.Size(180, 51);
            this.btnCopyAddressFromRelative.TabIndex = 0;
            this.btnCopyAddressFromRelative.TabStop = false;
            this.btnCopyAddressFromRelative.Text = "Copy Address from Relative";
            this.btnCopyAddressFromRelative.UseVisualStyleBackColor = true;
            this.btnCopyAddressFromRelative.Click += new System.EventHandler(this.button2_Click);
            // 
            // grpAddress
            // 
            this.grpAddress.Controls.Add(this.cboState);
            this.grpAddress.Controls.Add(this.txtCountry);
            this.grpAddress.Controls.Add(this.btnCopyAddressFromRelative);
            this.grpAddress.Controls.Add(this.txtAddress2);
            this.grpAddress.Controls.Add(this.lblCountry);
            this.grpAddress.Controls.Add(this.lblAddress1);
            this.grpAddress.Controls.Add(this.txtAddress1);
            this.grpAddress.Controls.Add(this.lblState);
            this.grpAddress.Controls.Add(this.lblAddress2);
            this.grpAddress.Controls.Add(this.txtCity);
            this.grpAddress.Controls.Add(this.lblZipcode);
            this.grpAddress.Controls.Add(this.lblCity);
            this.grpAddress.Controls.Add(this.txtZipcode);
            this.grpAddress.Location = new System.Drawing.Point(6, 94);
            this.grpAddress.Name = "grpAddress";
            this.grpAddress.Size = new System.Drawing.Size(571, 113);
            this.grpAddress.TabIndex = 1;
            this.grpAddress.TabStop = false;
            this.grpAddress.Text = "Home Address";
            // 
            // grpContactInfo
            // 
            this.grpContactInfo.Controls.Add(this.txtEmailAddress);
            this.grpContactInfo.Controls.Add(this.lblEmailAddress);
            this.grpContactInfo.Controls.Add(this.txtWorkPhone);
            this.grpContactInfo.Controls.Add(this.lblWorkPhone);
            this.grpContactInfo.Controls.Add(this.btnCopyContactFromRelative);
            this.grpContactInfo.Controls.Add(this.txtCellPhone);
            this.grpContactInfo.Controls.Add(this.lblHomePhone);
            this.grpContactInfo.Controls.Add(this.txtHomePhone);
            this.grpContactInfo.Controls.Add(this.lblCellPhone);
            this.grpContactInfo.Controls.Add(this.txtEmergencyContactPhone);
            this.grpContactInfo.Controls.Add(this.lblEmergencyContact);
            this.grpContactInfo.Controls.Add(this.lblEmergencyContactPhone);
            this.grpContactInfo.Controls.Add(this.txtEmergencyContact);
            this.grpContactInfo.Location = new System.Drawing.Point(6, 213);
            this.grpContactInfo.Name = "grpContactInfo";
            this.grpContactInfo.Size = new System.Drawing.Size(571, 136);
            this.grpContactInfo.TabIndex = 2;
            this.grpContactInfo.TabStop = false;
            this.grpContactInfo.Text = "Contact Information";
            // 
            // txtWorkPhone
            // 
            this.txtWorkPhone.Location = new System.Drawing.Point(273, 22);
            this.txtWorkPhone.Name = "txtWorkPhone";
            this.txtWorkPhone.Size = new System.Drawing.Size(96, 20);
            this.txtWorkPhone.TabIndex = 13;
            this.txtWorkPhone.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblWorkPhone
            // 
            this.lblWorkPhone.AutoSize = true;
            this.lblWorkPhone.Location = new System.Drawing.Point(200, 25);
            this.lblWorkPhone.Name = "lblWorkPhone";
            this.lblWorkPhone.Size = new System.Drawing.Size(67, 13);
            this.lblWorkPhone.TabIndex = 13;
            this.lblWorkPhone.Text = "Work Phone";
            // 
            // btnCopyContactFromRelative
            // 
            this.btnCopyContactFromRelative.Location = new System.Drawing.Point(379, 18);
            this.btnCopyContactFromRelative.Name = "btnCopyContactFromRelative";
            this.btnCopyContactFromRelative.Size = new System.Drawing.Size(178, 51);
            this.btnCopyContactFromRelative.TabIndex = 0;
            this.btnCopyContactFromRelative.TabStop = false;
            this.btnCopyContactFromRelative.Text = "Copy Contact Info from Relative";
            this.btnCopyContactFromRelative.UseVisualStyleBackColor = true;
            // 
            // txtCellPhone
            // 
            this.txtCellPhone.Location = new System.Drawing.Point(86, 49);
            this.txtCellPhone.Name = "txtCellPhone";
            this.txtCellPhone.Size = new System.Drawing.Size(96, 20);
            this.txtCellPhone.TabIndex = 12;
            this.txtCellPhone.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblHomePhone
            // 
            this.lblHomePhone.AutoSize = true;
            this.lblHomePhone.Location = new System.Drawing.Point(9, 25);
            this.lblHomePhone.Name = "lblHomePhone";
            this.lblHomePhone.Size = new System.Drawing.Size(69, 13);
            this.lblHomePhone.TabIndex = 11;
            this.lblHomePhone.Text = "Home Phone";
            // 
            // txtHomePhone
            // 
            this.txtHomePhone.Location = new System.Drawing.Point(86, 22);
            this.txtHomePhone.Name = "txtHomePhone";
            this.txtHomePhone.Size = new System.Drawing.Size(96, 20);
            this.txtHomePhone.TabIndex = 11;
            this.txtHomePhone.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblCellPhone
            // 
            this.lblCellPhone.AutoSize = true;
            this.lblCellPhone.Location = new System.Drawing.Point(9, 52);
            this.lblCellPhone.Name = "lblCellPhone";
            this.lblCellPhone.Size = new System.Drawing.Size(58, 13);
            this.lblCellPhone.TabIndex = 12;
            this.lblCellPhone.Text = "Cell Phone";
            // 
            // txtEmergencyContactPhone
            // 
            this.txtEmergencyContactPhone.Location = new System.Drawing.Point(423, 104);
            this.txtEmergencyContactPhone.Name = "txtEmergencyContactPhone";
            this.txtEmergencyContactPhone.Size = new System.Drawing.Size(133, 20);
            this.txtEmergencyContactPhone.TabIndex = 16;
            this.txtEmergencyContactPhone.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblEmergencyContact
            // 
            this.lblEmergencyContact.AutoSize = true;
            this.lblEmergencyContact.Location = new System.Drawing.Point(8, 107);
            this.lblEmergencyContact.Name = "lblEmergencyContact";
            this.lblEmergencyContact.Size = new System.Drawing.Size(100, 13);
            this.lblEmergencyContact.TabIndex = 15;
            this.lblEmergencyContact.Text = "Emergency Contact";
            // 
            // lblEmergencyContactPhone
            // 
            this.lblEmergencyContactPhone.AutoSize = true;
            this.lblEmergencyContactPhone.Location = new System.Drawing.Point(283, 107);
            this.lblEmergencyContactPhone.Name = "lblEmergencyContactPhone";
            this.lblEmergencyContactPhone.Size = new System.Drawing.Size(134, 13);
            this.lblEmergencyContactPhone.TabIndex = 16;
            this.lblEmergencyContactPhone.Text = "Emergency Contact Phone";
            // 
            // txtEmergencyContact
            // 
            this.txtEmergencyContact.Location = new System.Drawing.Point(120, 104);
            this.txtEmergencyContact.Name = "txtEmergencyContact";
            this.txtEmergencyContact.Size = new System.Drawing.Size(157, 20);
            this.txtEmergencyContact.TabIndex = 15;
            this.txtEmergencyContact.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblLevel
            // 
            this.lblLevel.AutoSize = true;
            this.lblLevel.Location = new System.Drawing.Point(9, 23);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(33, 13);
            this.lblLevel.TabIndex = 17;
            this.lblLevel.Text = "Level";
            this.lblLevel.Click += new System.EventHandler(this.label13_Click);
            // 
            // cboState
            // 
            this.cboState.FormattingEnabled = true;
            this.cboState.Location = new System.Drawing.Point(349, 75);
            this.cboState.Name = "cboState";
            this.cboState.Size = new System.Drawing.Size(53, 21);
            this.cboState.TabIndex = 9;
            // 
            // txtCountry
            // 
            this.txtCountry.Location = new System.Drawing.Point(457, 76);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(102, 20);
            this.txtCountry.TabIndex = 10;
            this.txtCountry.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cboCertifiedBy
            // 
            this.cboCertifiedBy.FormattingEnabled = true;
            this.cboCertifiedBy.Location = new System.Drawing.Point(286, 20);
            this.cboCertifiedBy.Name = "cboCertifiedBy";
            this.cboCertifiedBy.Size = new System.Drawing.Size(155, 21);
            this.cboCertifiedBy.TabIndex = 18;
            // 
            // txtDoB
            // 
            this.txtDoB.Location = new System.Drawing.Point(162, 48);
            this.txtDoB.Name = "txtDoB";
            this.txtDoB.Size = new System.Drawing.Size(110, 20);
            this.txtDoB.TabIndex = 4;
            this.txtDoB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtEmailAddress
            // 
            this.txtEmailAddress.Location = new System.Drawing.Point(86, 75);
            this.txtEmailAddress.Name = "txtEmailAddress";
            this.txtEmailAddress.Size = new System.Drawing.Size(284, 20);
            this.txtEmailAddress.TabIndex = 14;
            this.txtEmailAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblEmailAddress
            // 
            this.lblEmailAddress.AutoSize = true;
            this.lblEmailAddress.Location = new System.Drawing.Point(9, 78);
            this.lblEmailAddress.Name = "lblEmailAddress";
            this.lblEmailAddress.Size = new System.Drawing.Size(73, 13);
            this.lblEmailAddress.TabIndex = 14;
            this.lblEmailAddress.Text = "Email Address";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabInformation);
            this.tabControl1.Controls.Add(this.tabMembership);
            this.tabControl1.Controls.Add(this.tabStatus);
            this.tabControl1.Controls.Add(this.tabNotes);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(594, 443);
            this.tabControl1.TabIndex = 5;
            // 
            // tabInformation
            // 
            this.tabInformation.Controls.Add(this.grpPersonalInformation);
            this.tabInformation.Controls.Add(this.grpContactInfo);
            this.tabInformation.Controls.Add(this.grpCertifications);
            this.tabInformation.Controls.Add(this.grpAddress);
            this.tabInformation.Controls.Add(this.btnSubmit);
            this.tabInformation.Location = new System.Drawing.Point(4, 22);
            this.tabInformation.Name = "tabInformation";
            this.tabInformation.Padding = new System.Windows.Forms.Padding(3);
            this.tabInformation.Size = new System.Drawing.Size(586, 417);
            this.tabInformation.TabIndex = 0;
            this.tabInformation.Text = "Information";
            this.tabInformation.UseVisualStyleBackColor = true;
            // 
            // tabMembership
            // 
            this.tabMembership.Location = new System.Drawing.Point(4, 22);
            this.tabMembership.Name = "tabMembership";
            this.tabMembership.Padding = new System.Windows.Forms.Padding(3);
            this.tabMembership.Size = new System.Drawing.Size(586, 417);
            this.tabMembership.TabIndex = 1;
            this.tabMembership.Text = "Membership";
            this.tabMembership.UseVisualStyleBackColor = true;
            // 
            // tabStatus
            // 
            this.tabStatus.Controls.Add(this.dataGridView1);
            this.tabStatus.Location = new System.Drawing.Point(4, 22);
            this.tabStatus.Name = "tabStatus";
            this.tabStatus.Padding = new System.Windows.Forms.Padding(3);
            this.tabStatus.Size = new System.Drawing.Size(586, 417);
            this.tabStatus.TabIndex = 2;
            this.tabStatus.Text = "Status";
            this.tabStatus.UseVisualStyleBackColor = true;
            // 
            // tabNotes
            // 
            this.tabNotes.Location = new System.Drawing.Point(4, 22);
            this.tabNotes.Name = "tabNotes";
            this.tabNotes.Size = new System.Drawing.Size(586, 417);
            this.tabNotes.TabIndex = 3;
            this.tabNotes.Text = "Notes";
            this.tabNotes.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(9, 7);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(569, 139);
            this.dataGridView1.TabIndex = 0;
            // 
            // Form_Update_Customer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 443);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form_Update_Customer";
            this.Text = "Customer";
            this.grpPersonalInformation.ResumeLayout(false);
            this.grpPersonalInformation.PerformLayout();
            this.grpCertifications.ResumeLayout(false);
            this.grpCertifications.PerformLayout();
            this.grpAddress.ResumeLayout(false);
            this.grpAddress.PerformLayout();
            this.grpContactInfo.ResumeLayout(false);
            this.grpContactInfo.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabInformation.ResumeLayout(false);
            this.tabStatus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpPersonalInformation;
        private System.Windows.Forms.TextBox txtMiddle;
        private System.Windows.Forms.Label lblMiddle;
        private System.Windows.Forms.Label lblFirst;
        private System.Windows.Forms.GroupBox grpCertifications;
        private System.Windows.Forms.ComboBox cboLevel;
        private System.Windows.Forms.Label lblDoB;
        private System.Windows.Forms.TextBox txtLast;
        private System.Windows.Forms.Label lblLast;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.Label lblAddress1;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.Label lblAddress2;
        private System.Windows.Forms.Label lblZipcode;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.TextBox txtZipcode;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Button btnCopyAddressFromRelative;
        private System.Windows.Forms.GroupBox grpAddress;
        private System.Windows.Forms.GroupBox grpContactInfo;
        private System.Windows.Forms.Button btnCopyContactFromRelative;
        private System.Windows.Forms.TextBox txtCellPhone;
        private System.Windows.Forms.Label lblHomePhone;
        private System.Windows.Forms.TextBox txtHomePhone;
        private System.Windows.Forms.Label lblCellPhone;
        private System.Windows.Forms.TextBox txtEmergencyContactPhone;
        private System.Windows.Forms.Label lblEmergencyContact;
        private System.Windows.Forms.Label lblEmergencyContactPhone;
        private System.Windows.Forms.TextBox txtEmergencyContact;
        private System.Windows.Forms.Label lblCertifiedBy;
        private System.Windows.Forms.TextBox txtWorkPhone;
        private System.Windows.Forms.Label lblWorkPhone;
        private System.Windows.Forms.ComboBox cboCertifiedBy;
        private System.Windows.Forms.ComboBox cboState;
        private System.Windows.Forms.TextBox txtCountry;
        private System.Windows.Forms.TextBox txtFirst;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.Label lblLevel;
        private System.Windows.Forms.TextBox txtDoB;
        private System.Windows.Forms.TextBox txtEmailAddress;
        private System.Windows.Forms.Label lblEmailAddress;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabInformation;
        private System.Windows.Forms.TabPage tabMembership;
        private System.Windows.Forms.TabPage tabStatus;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabPage tabNotes;
    }
}