﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Webbing
{
    public partial class Form_Login : Form
    {

        public Form_Login()
        {
            InitializeComponent();
        }

        private void Form_Login_Load(object sender, EventArgs e)
        {
            txtPassword.UseSystemPasswordChar = false;

        }

        // Eventhandler to clear txtbox on first click
        private void txtUsername_clearOnFirstClick(object sender, EventArgs e)
        {
            if (txtUsername.Text == "Username") {
                txtUsername.Text = "";
            }
        }

        // Eventhandler to clear txtbox on first click
        private void txtPassword_clearOnFirstClick(object sender, EventArgs e)
        {
            if (txtPassword.Text == "Password") {
                txtPassword.Text = "";
                txtPassword.UseSystemPasswordChar = true;
            }
        }

        // Eventhandler to add helper text when blank
        private void txtUsername_textChanged(object sender, EventArgs e)
        {
            if (txtUsername.Text == "")
            {
                txtUsername.Text = "Username";
            }
        }

        // Eventhandler to add helper text when blank
        private void txtPassword_textChanged(object sender, EventArgs e)
        {
            if (txtPassword.Text == "")
            {
                txtPassword.Text = "Password";
                txtPassword.UseSystemPasswordChar = false;
            }
            else
            {
                txtPassword.UseSystemPasswordChar = true;
            }
        }

        // Eventhandler for login button
        // [!] Needs updating with DB-sided auth and valid sanitization
        private void btnLogin_Click(object sender, EventArgs e)
        {
            // [!] DEBUG BYPASS
            // User.userID = 2;
            this.Close(); /**/
            
            if (txtUsername.Text == "" || txtUsername.Text == "Username" || txtPassword.Text == "")
            {
                MessageBox.Show("Invalid username or password.", "Error");
                return;
            }

            if (txtUsername.Text == "BenjiFuse" && txtPassword.Text == "hey")
            {
                // User.userID = 2;
                this.Close();
            } else
            {
                MessageBox.Show("Invalid username or password.", "Error");
                return;
            }
        }
    }
}
