﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Webbing
{
    public partial class Form_Customers : Form
    {
        public Form_Customers()
        {
            InitializeComponent();
        }

        // On load, disable & hide form until a valid login occurs
        // [!] TODO: Actual Login Auth Logic (Argon2 check on DB for user)
        private void Form_Customers_Load(object sender, EventArgs e)
        {
            this.Hide();
            this.Enabled = false;
            using (Form_Login login = new Form_Login())
            {
                login.ShowDialog();
            }
            if (true) {
                this.Enabled = true;
                lblLogin.Text = "Logged in as Benji Fuse";
                lblLogin.ForeColor = Color.LimeGreen;
                this.Show();
            } else {
                this.Close();
            }
        }

        // Processes the provided query string
        public void CustomerSearch(String query)
        {
            // [!] TODO sanitiziation and validation of query from the Search Bar
            Database.Instance().customers_query(cid:4);
        }

        // Event Handler for Key Down (opens search tool)
        private void Form_Customers_KeyDown(object sender, KeyEventArgs e)
        {
            if (!char.IsLetter((char)e.KeyCode) && !char.IsDigit((char)e.KeyCode) && e.KeyCode != Keys.Enter) {
                return;         // do nothing for misc keys, only enter and alphanumeric trigger searchbar
            }

            char keychar = (char)e.KeyData;
            
            // Popup search box, passes ref to this form and first char
            using (Form_Customer_Search frm = new Form_Customer_Search(this, keychar))
            {
                frm.ShowDialog();
                frm.Focus();
            }
        }

        // Event handler to launch new customer form
        private void btnAddNewCustomer_Click(object sender, EventArgs e)
        {
            Customer new_customer = new Customer();
            using (Form form = new Form_Update_Customer(new_customer))
            {
                form.ShowDialog();
                form.Focus();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        // Temporary function to refresh the datagridview with the customer table data
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            // [!] DEBUG remove:
            //Database.Instance().add_new_customer("ben", "fuson", "kyle", "benjaminfuson4@gmail.com", "578 Emmer Drive", "", "Brandenburg", "KY", "USA", "(502) 378 - 2836", "(502) 378 - 2836", "lead belay", "1993-10-03");
            CustomerSearch("fuck");   // [!]
        }
    }
}
