﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Webbing
{
    public class Customer
    {
        public static List<Customer> customers = null;

        int CID = (-1); // Default CID is invalid
        int WID = 0;    // Default WID is 0 for none
        string first = "";
        string last = "";
        string middle = "";
        string address1 = "";
        string address2 = "";
        string zipcode = "";
        string city = "";
        string state = "";
        string country = "";
        string cellphone = "";
        string homephone = "";
        string workphone = "";
        string email = "";
        string emergencycontact = "";
        string emergencycontactphone = "";
        string certification = "";
        string certifiedby = "";
        DateTime? dob = null;

        // Default constructor
        public Customer()
        {
            // maintain updated list of all loaded customers:
            if (customers == null)
            {
                customers = new List<Customer>();
            }
            customers.Add(this);
        }

        // Constructor with optional parameters for each field of the object
        public Customer(int CID = -1, int WID = 0, string first = "", string middle = "", string last = "", 
            string address1 = "", string address2 = "", string zipcode = "", string city = "", string state = "", string country = "",
            string cellphone = "", string homephone = "", string workphone = "", string email = "", string emergencycontact = "",
            string emergencycontactphone = "", string certification = "", string certifiedby = "",  DateTime? dob = null)
        {
            this.first = first;
            this.middle = middle;
            this.last = last;
            this.address1 = address1;
            this.address2 = address2;
            this.zipcode = zipcode;
            this.city = city;
            this.state = state;
            this.country = country;
            this.cellphone = cellphone;
            this.homephone = homephone;
            this.workphone = workphone;
            this.email = email;
            this.emergencycontact = emergencycontact;
            this.emergencycontactphone = emergencycontactphone;
            this.certification = certification;
            this.certifiedby = certifiedby;
            this.dob = dob;
            // maintain updated list of all loaded customers:
            if (customers == null)
            {
                customers = new List<Customer>();
            }
            customers.Add(this);
        }

        // Deletes all currently loaded customers from the list
        public static void clear_customer_list()
        {
            if (customers != null) customers.Clear();
        }
    }
    
}
